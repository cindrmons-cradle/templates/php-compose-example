<?php

// EXAMPLE SQL CONNECTION

// credentials
$hostName = "db";
$hostUser = $_ENV["APPNAME"];
$password = $_ENV["MYSQL_USER_PASSWORD"];
$database = $_ENV["MYSQL_DB"];

// db connection
$dbct = new mysqli($hostName, $hostUser, $password, $database) or die("$dbct->connect_errno: $dbct->connect_error");

// example db query
$getPeopleQuery = "SELECT * FROM Person;";
$res = $dbct->query($getPeopleQuery);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title></title>
</head>
<body>
  <?php echo "Hello from PHP!"; ?>

  <h4>Database Connection Check: </h4>

  <?php
    // Check Connection
    if($dbct->ping() && $res) {
      printf("Connection Success!\n");
    ?>
    <p>Following data is from the DB: </p>
  <table>
    <tr>
      <th>Name</th>
      <th>Age</th>
      <th>Gender</th>
      <th>Bio</th>
    </tr>
  <?php while($row = $res->fetch_array(MYSQLI_ASSOC)) { ?>
    <tr>
      <td><?php echo $row["last_name"].", ".$row["first_name"]." ".$row["middle_name"]; ?></td>
      <td><?php echo $row["age"]; ?></td>
      <td><?php echo $row["gender"]; ?></td>
      <td><?php echo $row["bio"]; ?></td>
    </tr>
    <?php } ?>
  </table>
  <?php
    } 
    else {
      printf("Error! %s\n", $dbct->error);
    }

    $dbct->close();
  ?>

</body>
</html>
