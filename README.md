# DAMP Development Environment Template

- DAMP (Docker, Apache, MySQL, PHP) -- If you want to setup something like XAMPP, but you seem to want to use docker instead of legacy software.

- A basic reference for starting a LAMP stack dev environment using Docker as the main environment.

## Tech Stack

- Docker
- Apache HTTP
- PHP
- MariaDB/MySQL
- PHPMyAdmin

## Prerequisites

- For Windows:
    - [WSL/2 (Windows Subsystems for Linux)](https://learn.microsoft.com/en-us/windows/wsl/install)
    - [Docker Desktop](https://docs.docker.com/desktop/install/windows-install/)

- For Linux:
    - [Docker](https://docs.docker.com/desktop/install/linux-install/)
        > Additional Note for Linux: Also install the `docker compose` plugin for your distro.

## Setup

- Clone this project and delete the `.git` directory.
- Create a `.env` file by copying the `.env-example` file and pasting it as `.env`. Change the `MYSQL_ROOT_PASSWORD` if you choose to do so.

## How to Run Dev Environment

- Open a Terminal from project root.
- Run `docker compose up -d --build` to start up the development environment.

## How to Use Dev Envrionment

- PHP Entry point is in the `./html` directory. Add your PHP code there.
- Accessing PHPMyAdmin is accessed through this url: [http://localhost:9990/](http://localhost:9990/)
- Accessing running PHP code is accessed through this url: [http://localhost:9980/](http://localhost:9980/)

## Credits and Additional References

- [HowToForge, Dockerizing LEMP Stack with Docker Compose on Ubuntu](https://www.howtoforge.com/tutorial/dockerizing-lemp-stack-with-docker-compose-on-ubuntu/#step-configuration-of-the-dockercomposeyml-file)
- [github.com/stevenliebregt/docker-compose-lemp-stack](https://github.com/stevenliebregt/docker-compose-lemp-stack)

